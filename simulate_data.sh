#!/bin/bash
DB_NAME="spike"

CLICKHOUSE_SERVER="localhost:8123"
USERNAME=$(kubectl --context qa-qa1 --namespace perf-cash-testing get secret clickhouse-standalone-secrets -o jsonpath="{.data.clickhouse-admin-username}" | base64 --decode)
PASSWORD=$(kubectl --context qa-qa1 --namespace perf-cash-testing get secret clickhouse-standalone-secrets -o jsonpath="{.data.clickhouse-admin-password}" | base64 --decode)

function print() {
    local text="$1"
    local current_time=$(date +"%Y-%m-%d %H:%M:%S")
    echo "[$current_time] $text"
}

function insert_data() {
  DATA=""
  table_name=$1
  start=$2
  end=$3
  random_dec=$(printf '%.4f\n' "$(echo "scale=8; $RANDOM/32767*1000" | bc)")
  random_dec_1=$random_dec
  random_dec_2=$random_dec
  random_dec_3=$random_dec
  random_dec_4=$random_dec
  random_dec_5=$random_dec
  random_dec_6=$random_dec
  random_dec_7=$random_dec
  random_dec_8=$random_dec
  random_dec_9=$random_dec
  for ((i = start; i <= end; i++)); do
      account_group=$((start + RANDOM % (end - start + 1)))
      DATA+=" ($i, $account_group, $random_dec_1, $random_dec_2, $random_dec_3, $random_dec_4, $random_dec_5, $random_dec_6, $random_dec_7, $random_dec_8, $random_dec_9)"
      if [ "$i" -lt "$end" ]; then
          DATA+=","
      fi
  done
  INSERT_SQL="INSERT INTO $DB_NAME.\`${table_name}\` (id, account_group_id, amount_1, amount_2, amount_3, amount_4, amount_5, amount_6, amount_7, amount_8, amount_9) VALUES"
  clickhouse-client --server="$CLICKHOUSE_SERVER" --progress --user=${USERNAME} --password=${PASSWORD} --query="$INSERT_SQL$DATA;"
}

START_INDEX=1000
STEP=1000
PREV=$START_INDEX # acc group id

print "Starting..."
for i in {1..100} # repeat for each table n times
do
    for j in {1..150}  # for each table
    do
        table_name="cash-tx-${j}"
        start=$PREV
        end=$((PREV + STEP))
        insert_data $table_name $start "$end" & # insert n rows in separate process
        PREV=$((PREV + STEP + 1))
        if (( j % 150 == 0 )); then
          wait
          print "Round $i '/' $j done"
        fi
    done
    wait
    print "$i Round done"
    PREV=$START_INDEX
done

wait