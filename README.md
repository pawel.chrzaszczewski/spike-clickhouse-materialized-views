Install clickhouse client:
```shell
sudo apt install clickhouse-client
```

Verify materialized view created:
```shell
select database, name from system.tables where engine = 'MaterializedView'
```

Script `create_test_schema.sh` drops existing `spike` if exists and creates new one

Script `simulate_data.sh` fill source tables with data - two simple nested for loops:
1. for each n iteration
   2. for each table - run as a separate process

`clickhouse-client` have some limitation with sending data - therefore in one exec 1000 rows are inserted

Cumulative - 150 tables x 1000 rows x n iterations

Grafana dashboard for clickhouse: https://grafana.qa1.qa.duco.services/d/thEkJB_Mz/clickhouse-standalone?orgId=1&var-datasource=Prometheus&var-instance=All&var-trends=null&var-peaks=1&from=now-15m&to=now&refresh=30s