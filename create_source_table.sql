CREATE TABLE `spike`.`cash-tx`
(
    `id` Int64,
    `account_group_id` Int64,
    `amount_1` Nullable(Decimal(38, 8)),
    `amount_2` Nullable(Decimal(38, 8)),
    `amount_3` Nullable(Decimal(38, 8)),
    `amount_4` Nullable(Decimal(38, 8)),
    `amount_5` Nullable(Decimal(38, 8)),
    `amount_6` Nullable(Decimal(38, 8)),
    `amount_7` Nullable(Decimal(38, 8)),
    `amount_8` Nullable(Decimal(38, 8)),
    `amount_9` Nullable(Decimal(38, 8))
)
ENGINE = MergeTree
ORDER BY (id)
SETTINGS async_insert = 1;