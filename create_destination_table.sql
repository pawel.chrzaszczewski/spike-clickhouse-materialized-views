create table `spike`.`cash_account_group_proofs`
(
    `account_group_id` Int64,
    `proof_1` Decimal(38, 8),
    `proof_2` Decimal(38, 8),
    `proof_3` Decimal(38, 8),
    `proof_4` Decimal(38, 8),
    `proof_5` Decimal(38, 8),
    `proof_6` Decimal(38, 8),
    `proof_7` Decimal(38, 8),
    `proof_8` Decimal(38, 8),
    `proof_9` Decimal(38, 8)
)
engine = SummingMergeTree
order by (account_group_id);
