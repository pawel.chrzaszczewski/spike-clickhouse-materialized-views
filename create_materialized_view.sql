create materialized view `spike`.`cash-tx_to_cag_proofs_mv` to `spike`.`cash_account_group_proofs` as
select
    account_group_id,
    sum(amount_1) as proof_1,
    sum(amount_2) as proof_2,
    sum(amount_3) as proof_3,
    sum(amount_4) as proof_4,
    sum(amount_5) as proof_5,
    sum(amount_6) as proof_6,
    sum(amount_7) as proof_7,
    sum(amount_8) as proof_8,
    sum(amount_9) as proof_9
from `spike`.`cash-tx`
group by account_group_id;