# build source, destination, materialized views schemas

CREATE_SOURCE_SQL_FILE="create_source_table.sql"
CREATE_MV_SQL_FILE="create_materialized_view.sql"

CLICKHOUSE_SERVER="localhost:8123"
USERNAME=$(kubectl --context qa-qa1 --namespace perf-cash-testing get secret clickhouse-standalone-secrets -o jsonpath="{.data.clickhouse-admin-username}" | base64 --decode)
PASSWORD=$(kubectl --context qa-qa1 --namespace perf-cash-testing get secret clickhouse-standalone-secrets -o jsonpath="{.data.clickhouse-admin-password}" | base64 --decode)

clickhouse-client --server="$CLICKHOUSE_SERVER" --user=${USERNAME} --password=${PASSWORD} --query "drop database if exists spike"
clickhouse-client --server="$CLICKHOUSE_SERVER" --user=${USERNAME} --password=${PASSWORD} --query "create database spike"

clickhouse-client --server="$CLICKHOUSE_SERVER" --user=${USERNAME} --password=${PASSWORD} < create_destination_table.sql

for i in {1..150}
do
    table_name="cash-tx-${i}"
    SOURCE_SQL_QUERY=$(sed "s/cash-tx/${table_name}/g" "$CREATE_SOURCE_SQL_FILE")
    clickhouse-client --server="$CLICKHOUSE_SERVER" --user="$USERNAME" --password="$PASSWORD" --query "$SOURCE_SQL_QUERY"

    MV_SQL_QUERY=$(sed "s/cash-tx/${table_name}/g" "$CREATE_MV_SQL_FILE")
    clickhouse-client --server="$CLICKHOUSE_SERVER" --user="$USERNAME" --password="$PASSWORD" --query "$MV_SQL_QUERY"
done

